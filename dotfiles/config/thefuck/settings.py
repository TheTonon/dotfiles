# The Fuck settings file
#
# The rules are defined as in the example bellow:
#
# rules = ['cd_parent', 'git_push', 'python_command', 'sudo']
#
# The default values are as follows. Uncomment and change to fit your needs.
# See https://github.com/nvbn/thefuck#settings for more information.
#

# rules = [<const: All rules enabled>]
# debug = False
# exclude_rules = []
# instant_mode = False
# alter_history = True
# env = {'LC_ALL': 'C', 'GIT_TRACE': '1', 'LANG': 'C'}
# history_limit = None
# no_colors = False
# wait_slow_command = 15
# wait_command = 3
# require_confirmation = True
# slow_commands = ['lein', 'react-native', 'gradle', './gradlew', 'vagrant']
# repeat = False
# priority = {}
